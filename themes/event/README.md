# Tema Event para Hexo

Este es un tema para organizar evento usando hexo

## Features
- Mapa para localizar el evento
- Disqus and Facebook comments
- Google Analytics
- Addthis
- Cover image for posts and pages
- Tags and Categories Support
- Responsive Images
- Image Gallery
- Code syntax highlighting

### trabajando en
- Agenda
- Lista de oradores
- Lista para Sponsors
- Galeria para fotos del evento (post evento)

## Bibliotecas externas

- [Bootstrap](http://getbootstrap.com/css/)
- [FeatherLight.js](http://noelboss.github.io/featherlight/) (Gallery)
- [jQuery](https://jquery.com/)
- [Fontawesome](https://fontawesome.com/)

## Instalacion

```
$ git clone https://gitlab.com/hackatory/event themes/event
```

Luego cambia tu `_config.yml` para usar el tema `event`:

```
# Extensions
## Plugins: http://hexo.io/plugins/
## Themes: http://hexo.io/themes/
theme: event
```

## Configuracion

Las siguientes opciones se pueden modificar en el archivo `_config.yml` dentro
de la carpeta del tema.

### Evento
#### lugar
#### dia
#### horario
#### mapa

### Menu

```
# Header
menu:
  Inicio: /
  Archivos: /archives
  Github:
    url: https://gitlab.com/hackatory/event
    icon: github
```

### Top Left Label

```
# Title on top left of menu. Leave empty to use main blog title
menu_title: Configurable Title
```

### Home Page cover image

```
# URL of the Home page image
index_cover: /img/home-bg.jpg
```

### Default post title

```
# Default post title
default_post_title: Untitled
```

### Comments

The comments provider is specified in the theme's `_config.yml`. If you specify
both a `disqus_shortname` and a `facebook.appid` there will be 2 sets of
comment per post. So choose one.

```
# Comments. Choose one by filling up the information
comments:
  # Disqus comments
  disqus_shortname: klugjotest
  # Facebook comments
  facebook:
    appid: 123456789012345
    comment_count: 5
    comment_colorscheme: light
```

You can too hide the comment in the posts front-matter:

```
comment: false
---
```

### Google Analytics

The Google Analytics Tracking ID is configured in the theme's `_config.yml`.

```
# Google Analytics Tracking ID
google_analytics:
```

### Addthis

The Addthis ID is configured in the theme's `_config.yml`.

```
# Addthis ID
addthis:
```

### Social Account

Setup the links to your social pages in the theme's `_config.yml`. Links are in the footer.

```
# Social Accounts
twitter_url:
facebook_url:
github_url: https://gitlab.com/hackatory/event
linkedin_url:
mailto:
```

### Author

The post's author is specified in the posts front-matter:

```
author: Klug Jo
---
```

### Post's Cover Image

By default, posts will use the home page cover image. You can specify a custom cover in the front-matter:

```
title: Excerpts
date: 2013-12-25 00:23:23
tags: ["Excertps"]
cover: /assets/contact-bg.jpg
---
```

### Post's Share Cover Image

You can specify a custom cover to share yours posts in social medias:

```
share_cover: /assets/contact-bg.jpg
---
```

### Post's Excerpt

This theme does not support traditional excerpts. To show excerpts on the index page, use `subtitle` in the front-matter:

```
title: Excerpts
date: 2013-12-25 00:23:23
tags: ["Excertps"]
subtitle: Standard Excerpts are not supported in Clean Blog but you can use subtitles in the front matter to display text in the index.
---

```

## Tags page.

> Follow these steps to add a `tags` page that contains all the tags in your site.

- Create a page named `tags`

```
$ hexo new page "tags"
```

- Edit the newly created page and set page type to `tags` in the front matter.

```
title: All tags
type: "tags"
```

- Add `tags` to the menu in the theme `_config.yml`:

```
# Header
menu:
  Home: /
  Archives: /archives
  Tags: /tags
```

## Categories page.

> Follow these steps to add a `categories` page that contains all the categories in your site.

- Create a page named `categories`

```
$ hexo new page "categories"
```

- Edit the newly created page and set page type to `categories` in the front matter.

```
title: All tags
type: "categories"
```

- Add `Categories` to the menu in the theme `_config.yml`:

```
# Header
menu:
  Home: /
  Archives: /archives
  Categories: /categories
```

## License

GPL V3
